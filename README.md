# Development of Printed Circuit Boards at FabLab Brno

Experiments on figuring out how to make Printed Circuit Boards ("PCBs") using the tools and resources at FabLab for an Open-Source Hardware Development, namely development of prototypes for [OLIMEX Teres-A64](https://github.com/OLIMEX/DIY-LAPTOP) and [KREYLIMEX DEIMOS-A64](https://git.dotya.ml/kreyren/OSHW-DEIMOS).

### Method 1: Laser

See laser-method.md 

#### Material cutting tests

TODO(Krey): Perform a material cutting test on standard blank copper FR-4 board
1. Engraving in the copper
2. Evaporating copper
3. Evaporating Fibreglass
4. Evaporating Copper and Fibreglass at the same time (to release the PCBs from the blank and possibly make drill holes)

Projected hazards:
1. Material catching fire
2. Material reflecting laser beam
3. Material releasing toxic fumes

Due to concerns of the material cathing fire it should be only performed at size that can burn inside the machine without causing major fire or damaging the machine such as 5x30 mm.

TODO(Krey): Perform a burn test by coating the material in an isopropyl alcohol 99%, flashing it and then recording the result, the material is projected to be self-extinquishing.

#### Verify accuracy of the fabrication process

TODO(Krey): Prepare a benchmark

## References

1. Seemingly a good resource on using a photoresistive board to make PCBs -- https://www.youtube.com/watch?v=sPnnOcxqUhc