This file is exporing the options of using laser to make Printed Circuit Boards at FabLab Brno where the projected path of least resistance is to use the laser cutter to cut the PCBs as it's the fastest and most resource efficient method in comparison to engraving traces and then eating away the unwanted copper with an etching solution such as Ferric Chloride (FeCl3) or using CNC which additionally requires special tools namely [drill bit with 0.5mm diameter](https://www.amanatool.com/products/cnc-router-bits/cnc-3d-carving-router-bits/48418-k-m-cnc-2d-and-3d-carving-3-7-deg-tapered-angle-ball-tip-x-50mm-dia-x-25mm-radius-x-19mm-x-3mm-shank-x-75mm-long-x-3-flute-solid-carbide-up-cut-spiral-spektratm-extreme-tool-life-coated-router-bit) and lot of tinkering with CNC with results that potentially cannot compare with laser in terms of precision and width of the cut.

As of 01.05.2023-EU the makerspace has two industrial laser cutting machines:
* Trotec Speedy 300
* Trotec Speedy 400

Both using CO2 Laser.

According to the experiments by Saheen Palayi from FabLab Acedemy 2021 the Trotec Speedy 400 with a CO2 laser is able to cut PCBs after the fibre laser has done engraving of the copper layer using the following settings[5]:
![](http://fabacademy.org/2021/labs/kochi/students/saheem-palayi/images/week4/laser-engraving-setting.png)

The next step should then be to confirming the results.

In terms of safety the concern over the laser reflecting and damaging the machine was highlighted to me where the Trotec itself clarifies in reference 6 that for the laser to reflect it has to be special super polished metal mirror and reminding that the laser needs to be focused in order to cut where reflection would disperse it's beam rendering it useless.

Another potential risk highlighted was that cutting such material may release toxic chemicals, but according to the reference 7 such toxicity was not observed.

Another potential risk was fire where reference 5 shows using the same machine without an issue and reference 8 by firefighting website clarifies that:

> Like most metals, copper is solid and while it has a relatively low melting point for a metal, but it does not ignite at commonly encountered temperatures and is therefore not flammable. [8]

It does mention the risk of flame if it's set to dust particles which can be seen on reference 9, but such scenario is considered as not relevant for the reasons explained to the use on the laser cutter. Just as a safety measure when performing the test I will have a D class fire extinquisher ready.

Additionally fibreglass is considered as neither flammable nor combustible. [10]

Requested permission to perform experiment with cutting copper in the laser to CORE team.

Request was denied with the following reasoning:
> Díval jsem se na git, procházel odkazy, máš to pěkně zpracovaný, ale bohužel to povolit nemůžeme.
> 
> -Saheen Palayi používá fiber laser, gravíruje tedy vláknem ne CO2.
> -V trotec videu, na které odkazuješ [6] říkají, že není problém gravírovat zrcadlo, pokud je pod sklem/plexisklem, kdy se gravíruje do skla/plexiskla. Do zrcadla se smí gravírovat pouze z druhé strany, kterou nemá lesklou. U lesklých povrchů, typický kovů hrozí poškození stroje.
> -Happylab ve Vídni (mají 3 laby, přes 2000 členů a napáleno řádově víc hodin a projektů na laserech než my) má zakázané jak FR, tak měď https://wiki-happylab-at.translate.goog/w/Nicht_geeignete_Materialien?_x_tr_sl=cs&_x_tr_tl=en&_x_tr_hl=cs&_x_tr_pto=wapp

Reference 11 and 12 has been added to the engraving and cutting metals where trotec claims that:
> In general, it is possible to cut metal with both CO2 and fiber laser. Metals such as steel or ferrous materials can be cut more easily than light or non-ferrous metals such as copper or aluminum.[12]

Trotec has been contacted for a quote on whether it is safe to work with copper in the laser just in case these references are not enough.

Saheem Palayi is using fibre laser for engraving. It is unlikely that CO2 would be able to cut copper of thickness 17 to 221mu, but I want to experiment with it as trotec seems to claim that CO2 is able to engrave the copper so there are probably ways that should be explored.

Optionally spray paint coating the PCB with a paint and then removing the paint on the CO2 laser so that it can be then etched would also be a valid usecase that would enable development of printed circuit boards that is superior to CNC milling considering the precision and cutting diameter[13], but it is likely that we will need the ability to inject custom GCODE in the laser to get the required performance which should be addressed in https://git.dotya.ml/kreylab-brno/gcoding-the-speeedeeeey.

About happylab I talked with a self-proclaimed member and they seem to believe that none there seems to even know where to start in terms of designing and fabricating PCBs compared to us being literally in the VUT campus (VUT is a well known college focused on computer science and electrical engineering among other things).
So it's likely that results from this experiment would enable such production in vienna and/or them asking us to make PCBs for them (which they seem to have an interest in already) and I am willing to assist them in configuring their infrastructure if desired as I am going to vienna on semi-often bases.

### References

1. fablab academy page on cutting PCBs with a CO2/fibre trotec laser cutter -- https://fabacademy.org/archives/2015/doc/fiber-laser-cutting-pcb.html
2. Video of trotec speedy 100 FLEXX engraving and cutting the PCB -- https://www.youtube.com/watch?v=pXH49ZWdGrA
3. Process by berytech fablab using CNC mill -- http://fabacademy.org/2019/labs/berytech/Electronics_Production.html
4. Fablab academy page on fabricating electronical products -- http://fabacademy.org/2021/labs/kochi/students/saheem-palayi/assignments/week4.html
5. Video of fabricating PCBs on Trotec Speedy 400 using CO2 laser -- https://youtu.be/bl8-EGnb928
6. Video by trotec on cutting mirrors -- https://www.youtube.com/watch?v=N93rjDzbDbQ
7. Safety data sheet of FR4 boards -- https://docs.rs-online.com/81c4/0900766b8030ec09.pdf
8. Firefighter insider reference on flamability of copper -- https://firefighterinsider.com/copper-flammable
9. Copper fire test -- https://www.youtube.com/watch?v=dwsexjcROH4
10. Safety data sheet on fibreglass -- https://www.gprdirect.com/1/2/f/1/sds-glass-fibre.pdf
11. Trotec reference on engraving metals -- https://www.troteclaser.com/en/laserable-materials/laser-engraving-metal
12. Trotec reference on cutting metals -- https://www.troteclaser.com/en/learn-support/faqs/laser-processing-metal
13. Reference on making PCBs by spray coating them with paint and removing the paint layer with laser for etching -- https://www.youtube.com/watch?v=1hFNj86L7sk
13.1. 6:25 mentions the process of using spray paint and laser for etching